<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 02.03.2017
 * Time: 19:55
 */
namespace Macrocrm;

class Client
{

    const USER_AGENT = 'MacrocrmApi';
    const VERSION = '1.1';
    const SOCKET_TIMEOUT = 60;

    /**
     * @var string домен API
     */
    private $api_domain = 'api.macrocrm.ru';

    /**
     * @var string ключ клиента
     */
    private $app_secret;

    /**
     * @var string домен клиента
     */
    private $domain;

    /**
     * @var string способ соединения с сервером
     */
    private $fetch_remote_type;

    /**
     * Хранит последние данные, возвращенные api
     *
     * @var mixed
     */
    public $last_data_fetched;

    /**
     * Хранит последний запрошенный URL
     *
     * @var string
     */
    public $last_url_fetched;

    public $simple_token = true;

    function __construct($domain, $app_secret, $api_domain = null)
    {
        $this->setDomain($domain);
        $this->setAppSecret($app_secret);
        if ($api_domain) {
            $this->setApiDomain($api_domain);
        }
    }

    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    public function setAppSecret($app_secret)
    {
        $this->app_secret = $app_secret;
    }

    public function setApiDomain($api_domain)
    {
        $this->api_domain = $api_domain;
    }

    /**
     * Подготавливает параметризированную ссылку для запроса к API
     *
     * @param $path
     * @param $params
     *
     * @return string
     */
    public function get_api_url($path, $params)
    {
        $this->_tokenize($params);
        return 'https://' . $this->api_domain . '/' . trim(dirname($path) . '/' . basename($path), '/') . '?' . http_build_query($params);
    }

    /**
     * Запрос к API
     *
     * @param string $path
     * @param array  $params
     * @param string $data_type
     *
     * @return mixed|null
     */
    public function api($path, $params = array())
    {
        $url = $this->get_api_url($path, $params);
        $data = $this->fetch_url($url);
        $this->last_data_fetched = $data;
        $this->last_url_fetched = $url;

        return $data ? @json_decode($data, true) : null;
    }


    /**
     * Осуществление запроса на сервер
     *
     * @param $url
     *
     * @return bool|mixed|string
     */
    private function fetch_url($url)
    {

        $user_agent = self::USER_AGENT . ' ' . self::VERSION;
        @ini_set('allow_url_fopen', 1);
        @ini_set('default_socket_timeout', self::SOCKET_TIMEOUT);
        @ini_set('user_agent', $user_agent);

        if ($this->fetch_remote_type == '' && function_exists('file_get_contents') && ini_get('allow_url_fopen') == 1) {
            return @file_get_contents($url);
        } elseif (
            $this->fetch_remote_type == 'curl'
            ||
            (
                $this->fetch_remote_type == ''
                &&
                function_exists('curl_init')
            )
        ) {
            $this->fetch_remote_type = 'curl';
            if ($ch = @curl_init()) {

                @curl_setopt($ch, CURLOPT_URL, $url);
                @curl_setopt($ch, CURLOPT_HEADER, false);
                @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                @curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::SOCKET_TIMEOUT);
                @curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

                $data = @curl_exec($ch);
                @curl_close($ch);

                return $data;
            }
        } else {
            $port = 80;
            $extra_headers = array();
            $url = strtr(strval($url), array("http://" => "", "https://" => "ssl://", "ssl://" => "ssl://", "\\" => "/", "//" => "/"));

            if (($protocol = stripos($url, "://")) !== FALSE) {
                if (($domain_pos = stripos($url, "/", ($protocol + 3))) !== FALSE) {
                    $domain = substr($url, 0, $domain_pos);
                    $file = substr($url, $domain_pos);
                } else {
                    $domain = $url;
                    $file = "/";
                }
            } else {
                if (($domain_pos = stripos($url, "/")) !== FALSE) {
                    $domain = substr($url, 0, $domain_pos);
                    $file = substr($url, $domain_pos);
                } else {
                    $domain = $url;
                    $file = "/";
                }
            }

            $fp = fsockopen($domain, $port, $errno, $errstr, 30);
            if (!$fp) {
                return FALSE;
            } else {
                $out = "GET " . $file . " HTTP/1.1\r\n";
                $out .= "Host: " . $domain . "\r\n";
                foreach ($extra_headers as $nm => $vl) {
                    $out .= strtr(strval($nm), array("\r" => "", "\n" => "", ": " => "", ":" => "")) . ": " . strtr(strval($vl), array("\r" => "", "\n" => "", ": " => "", ":" => "")) . "\r\n";
                }
                $out .= "Connection: Close\r\n\r\n";

                $response = "";
                fwrite($fp, $out);
                while (!feof($fp)) {
                    $response .= fgets($fp, 128);
                }
                fclose($fp);

                global $http_response_header;
                $http_response_header = array();

                if (stripos($response, "\r\n\r\n") !== FALSE) {
                    $hc = explode("\r\n\r\n", $response);
                    $headers = explode("\r\n", $hc[0]);

                    if (!is_array($headers)) $headers = array();
                    foreach ($headers as $key => $header) {
                        $a = "";
                        $b = "";
                        if (stripos($header, ":") !== FALSE) {
                            list($a, $b) = explode(":", $header);
                            $http_response_header[trim($a)] = trim($b);
                        }
                    }
                    return end($hc);
                } elseif (stripos($response, "\r\n") !== FALSE) {
                    $headers = explode("\r\n", $response);

                    if (!is_array($headers)) $headers = array();
                    foreach ($headers as $key => $header) {
                        if ($key < (count($headers) - 1)) {
                            $a = "";
                            $b = "";
                            if (stripos($header, ":") !== FALSE) {
                                list($a, $b) = explode(":", $header);
                                $http_response_header[trim($a)] = trim($b);
                            }
                        }
                    }
                    return end($headers);
                } else {
                    return $response;
                }
            }
        }
    }

    /**
     * Цифровая подпись данных
     *
     * @param array $data
     */
    private function _tokenize(array &$data)
    {
        $data['domain'] = $this->domain;
        $data['time'] = time();
        $data['token'] = $this->app_secret;
        $data['token'] = $this->hash($data, $this->app_secret);
    }

    /**
     * Последовательно хэширование массив данных, избегая пустых значений,
     * которые будут потеряны при передаче через GET
     *
     * @param $data
     * @param $key
     *
     * @return string
     */
    private function hash($data, $key)
    {
        if ($this->simple_token){
            return md5($data['domain'].$data['time'].$key);
        } else {
            unset($data['token']);
            $ctx = hash_init('md5', HASH_HMAC, $key);
            $this->hash_proc($data, $ctx);
            return hash_final($ctx);
        }
    }

    private function hash_proc($value, $ctx)
    {
        if (is_array($value)) {
            foreach ($value as $key => $val) {
                if (!empty($val)) {
                    hash_update($ctx, $key);
                    $this->hash_proc($val, $ctx);
                }
            }
        } elseif (!empty($value)) {
            hash_update($ctx, $value);
        }
    }

}