<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 23.10.2017
 * Time: 11:31
 */

namespace Macrocrm;

use \PDO;
use \PDOStatement;

class Inventory {

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var PDO
     */
    protected $dbhandler;

    /**
     * @var string Имя таблицы в которой будет храниться ТМЦ
     */
    protected $_table_name;

    protected $debugFunc;

    function __construct(Client $client,PDO $PDO,$table_name='inventory')
    {
        $this->client=$client;
        $this->dbhandler = $PDO;
        $this->_table_name = $table_name;
    }

    /**
     * Получение существующей схемы основной таблицы
     *
     * @return array
     */
    protected function get_columns($table_name = null)
    {
        if (is_null($table_name)) {
            $table_name = $this->_table_name;
        }
        $cols = array();
        foreach ($this->query('show columns from ' . $this->quoteIdent($table_name)) as $col) {
            $cols[$col['Field']] = preg_replace('~[^a-z ]~', '', $col['Type']);
        }
        return $cols;
    }

    /**
     * Проверка наличия таблицы в базе
     *
     * @param string $table_name
     *
     * @return bool
     */
    function tableExists($table_name)
    {
        $mrSql = "SHOW TABLES LIKE :table_name";
        $mrStmt = $this->dbhandler->prepare($mrSql);
        //protect from injection attacks
        $mrStmt->bindParam(":table_name", $table_name, PDO::PARAM_STR);

        $sqlResult = $mrStmt->execute();
        if ($sqlResult) {
            $row = $mrStmt->fetch(PDO::FETCH_NUM);
            if ($row[0]) {
                //table was found
                return true;
            } else {
                //table was not found
                return false;
            }
        } else {
            //some PDO error occurred
            return false;
        }
    }

    /**
     *  Удаление таблицы
     */
    protected function dropTable($table_name)
    {
        $this->dbhandler->exec("DROP TABLE IF EXISTS " . $this->quoteIdent($table_name));
    }

    /**
     * Генерация таблицы inventory из api + синхронизация данных
     */
    function sync_inventory()
    {
        $table = $this->_table_name;
        # Если таблица не существует - создаем
        $this->debug($table.' syncing...');
        if (!$this->tableExists($table)) {
            $this->dropTable($table);
            $ddl = 'CREATE TABLE ' . $this->quoteIdent($table) . ' (
            id int(11) unsigned NOT NULL,
            inventory_demands_id int(11) unsigned NOT NULL,
            noms_id int(11) unsigned,
            demander_id int(11) unsigned,
            item_quantity  float(20) unsigned,
            item_price float(20) unsigned,
            item_summa float(20) unsigned,
            item_summa_nds float(20) unsigned,
            item_measure varchar(32),
            date_demand int(11) unsigned,
            status int(11) unsigned,
            noms_name varchar(32),
            req_id int(11) unsigned,
            req_contacts_id int(11) unsigned,
            req_status int(11) unsigned,
            req_demander_id int(11) unsigned,
            req_supplier_id int(11) unsigned,
            req_description varchar(1024),
            req_supplier_comment varchar(1024),
            req_supplier_name varchar(32),
            PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8';
            $result = $this->dbhandler->exec($ddl);
        }

        # Тянем данные из объектов
        $api_result = $this->client->api('inventory/demands/getList');
        if (empty($api_result)) {
            if (is_array($api_result)){
                $this->debug('Empty data from server');
                return false;
            }
            throw new Exception('Server returned no data');
        }
        if (isset($api_result['code'], $api_result['message'])) {
            throw new Exception('Server error: ' . $api_result['message']);
        }

        $data = [];

        # Убираем вложенность массива данных, меняем структуру относительно самих ТМЦ
        foreach ($api_result as $result_item) {
            foreach ($result_item['items'] as $item) {
                $item['date_demand'] = strtotime($item['date_demand']);
                $data[] = array_merge($item, [
                    'req_id' => $result_item['id'],
                    'req_contacts_id' => $result_item['contacts_id'],
                    'req_status' => $result_item['status'],
                    'req_demander_id' => $result_item['demander_id'],
                    'req_supplier_id' => $result_item['supplier_id'],
                    'req_description' => $result_item['description'],
                    'req_supplier_comment' => $result_item['supplier_comment'],
                    'req_supplier_name' => $result_item['supplier_name'],
                ]);
            }

        }

        # Получаем поля таблицы в базе
        $columns = $this->get_columns($table);

        # Собираем ID всех записей, имеющихся в базе
        $exists_query = $this->dbhandler->query("select id from " . $table . " order by null");
        $exists = $exists_query->fetchAll();

        # Приводим собранные ID к удобному виду
        $exists_vals = array();
        foreach ($exists as $exists_val) {
            $exists_vals[] = (int)$exists_val['id'];
        }

        # Инициализируем массив записей для добавления в базу, где ключом является ID
        $toInsert = array();


        # Заполняем массив вставки в нужном формате
        foreach ($data as $file) {
            if (!isset($file['id'])) {
                throw new Exception('Error data');
            }
            $toInsert[$file['id']] = array_intersect_key($file, $columns);
        }


        # Прерываем выполнение, если данные не пришли
        if (empty($toInsert)) {
            $this->debug('no date');
            return false;
        }

        # Формируем массив ID записей для удаления и удаляем из базы лишнее
        $toDelete = array_diff($exists_vals, array_keys($toInsert));
        if (!empty($toDelete)) {
            $toDelete_string = implode(',', $toDelete);
            $delete_query = "delete from " . $table . " where id in (" . $toDelete_string . ")";
            $this->dbhandler->exec($delete_query);
        }


        # Обновляем записи в базе

        # Формируем поля для вставки в запрос
        $fields = '`' . implode('`,`', array_keys(reset($toInsert))) . '`';

        # Формируем строку для вставки в запрос при обновлении элемента
        $toUpdate_items_on_duplicate_array = array();
        $toUpdate_items = array_keys(reset($toInsert));
        foreach ($toUpdate_items as $toUpdate_item) {
            $toUpdate_items_on_duplicate_array[] = "`" . $toUpdate_item . "`=VALUES(`" . $toUpdate_item . "`)";
        }
        $toUpdate_items_on_duplicate = implode(' ,', $toUpdate_items_on_duplicate_array);


        # Производим добавление и обновление записей в базе
        foreach ($toInsert as $toInsert_item_array) {
            # Теперь добавляем и обновляем записи
            $toInsert_item_array_slashed = array();
            foreach ($toInsert_item_array as $toInsert_item_array_item) {
                $toInsert_item_array_slashed[] = addslashes($toInsert_item_array_item);
            }
            $toInsert_item = '"' . implode('","', $toInsert_item_array_slashed) . '"';
            $updated = $this->dbhandler->exec("insert into " . $table . " (" . $fields . ") VALUES (" . $toInsert_item . ") ON DUPLICATE KEY UPDATE " . $toUpdate_items_on_duplicate );
        }
        $this->debug('updated '.$updated);

        return true;
    }

    /**
     * Прямой запрос в базу данных
     *
     * @param $query
     *
     * @return array
     */
    function query($query)
    {
        $data = $this->dbhandler->query($query);
        return $data == false ? array() : $data->fetchAll(PDO::FETCH_ASSOC);
    }

    protected function quoteIdent($field)
    {
        return "`" . str_replace("`", "``", $field) . "`";
    }

    function debug()
    {
        $data = func_get_args();
        if (is_callable($this->debugFunc)) {
            call_user_func_array($this->debugFunc, $data);
        }
    }

    function setDebug($callable)
    {
        $this->debugFunc = $callable;
    }

}