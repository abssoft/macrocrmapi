<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 23.10.2017
 * Time: 11:31
 */

namespace Macrocrm;

use \PDO;
use \PDOStatement;

class Facades {

    /**
     * @var PDO
     */
    protected $dbhandler;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string Имя таблицы в которой будет храниться объекты
     */
    protected $_table_name;

    protected $debugFunc;

    function __construct(Client $client, PDO $PDO, $table_name = 'objects_facades')
    {
        $this->client = $client;
        $this->dbhandler = $PDO;
        $this->_table_name = $table_name;
    }

    /**
     * Проверка наличия таблицы в базе
     *
     * @param string $table_name
     *
     * @return bool
     */
    function tableExists($table_name)
    {
        $mrSql = "SHOW TABLES LIKE :table_name";
        $mrStmt = $this->dbhandler->prepare($mrSql);
        //protect from injection attacks
        $mrStmt->bindParam(":table_name", $table_name, PDO::PARAM_STR);

        $sqlResult = $mrStmt->execute();
        if ($sqlResult) {
            $row = $mrStmt->fetch(PDO::FETCH_NUM);
            if ($row[0]) {
                //table was found
                return true;
            } else {
                //table was not found
                return false;
            }
        } else {
            //some PDO error occurred
            return false;
        }
    }

    /**
     * Получение существующей схемы основной таблицы
     *
     * @return array
     */
    protected function get_columns($table_name = null)
    {
        if (is_null($table_name)) {
            $table_name = $this->_table_name;
        }
        $cols = array();
        foreach ($this->query('show columns from ' . $this->quoteIdent($table_name)) as $col) {
            $cols[$col['Field']] = preg_replace('~[^a-z ]~', '', $col['Type']);
        }
        return $cols;
    }

    /**
     * Прямой запрос в базу данных
     *
     * @param $query
     *
     * @return array
     */
    function query($query)
    {
        $data = $this->dbhandler->query($query);
        return $data == false ? array() : $data->fetchAll(PDO::FETCH_ASSOC);
    }

    protected function quoteIdent($field)
    {
        return "`" . str_replace("`", "``", $field) . "`";
    }

    function debug()
    {
        $data = func_get_args();
        if (is_callable($this->debugFunc)) {
            call_user_func_array($this->debugFunc, $data);
        }
    }

    function setDebug($callable)
    {
        $this->debugFunc = $callable;
    }

    /**
     *  Удаление таблицы
     */
    protected function dropTable($table_name)
    {
        $this->dbhandler->exec("DROP TABLE IF EXISTS " . $this->quoteIdent($table_name));
    }

    /**
     * Синнхронизация схемы основной таблицы
     * При наличии изменений пересоздает таблицу
     * Сохранение файлов CRM на локальном диске
     * Добавление и обновление элементов
     */
    function sync($files_dir)
    {
        $table = $this->_table_name;
        # Если таблица не существует - создаем
        $this->debug($table . ' syncing...');
        if (!$this->tableExists($table)) {
            $this->dropTable($table);
            $ddl = 'CREATE TABLE ' . $this->quoteIdent($table) . ' (
            id int(11) unsigned NOT NULL,
            estate_id int(11) unsigned NOT NULL,
            name varchar(255) NOT NULL,
            entrance varchar(32) NOT NULL,
            file_name varchar(255) NOT NULL,
            file_ext varchar(3) NOT NULL,
            file_url varchar(255) NOT NULL,
            date_added timestamp NOT NULL,
            PRIMARY KEY (id), INDEX(estate_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8';
            $this->dbhandler->exec($ddl);
        }
      
        # Тянем данные из API
        $data = $this->client->api('estate/group/getFacades', array());
        if (empty($data)) {
            if (is_array($data)) {
                $this->debug('Empty data from server');
                return false;
            }
            throw new Exception('Server returned no data');
        }
        if (isset($data['code'], $data['message'])) {
            throw new Exception('Server error: ' . $data['message']);
        }

        # Собираем ID всех записей, имеющихся в базе
        $exists = $this->query("select id,file_url,date_added from " . $table . " order by null");

        # Приводим собранные ID к удобному виду
        $exists_vals = array();
        foreach ($exists as $exists_val) {
            $exists_vals[$exists_val['id']] = $exists_val;
        }

        # Инициализируем массив записей для добавления в базу, где ключом является ID
        $toInsert = [];
        foreach ($data as $file) {
            if (!isset($file['id'])) {
                throw new Exception('Error data');
            }
            $toInsert[$file['id']] = $file;
        }

        # Прерываем выполнение, если данные не пришли
        if (empty($toInsert)) {
            $this->debug('no date');
            return false;
        }

        # Формируем массив ID записей для удаления и удаляем из базы лишнее
        $toDelete = array_diff(array_keys($exists_vals), array_keys($toInsert));
        if (!empty($toDelete)) {
            $toDelete_string = implode(',', $toDelete);
            $delete_query = "delete from " . $table . " where id in (" . $toDelete_string . ")";
            $this->dbhandler->exec($delete_query);
        }



        # Обновляем записи в базе

        # Формируем поля для вставки в запрос
        $fields = '`' . implode('`,`', array_keys(reset($toInsert))) . '`';

        # Формируем строку для вставки в запрос при обновлении элемента
        $toUpdate_items_on_duplicate_array = array();
        $toUpdate_items = array_keys(reset($toInsert));
        foreach ($toUpdate_items as $updateKey => $toUpdate_item) {
            $toUpdate_items_on_duplicate_array[] = "`" . $toUpdate_item . "`=VALUES(`" . $toUpdate_item . "`)";
        }
        $toUpdate_items_on_duplicate = implode(' ,', $toUpdate_items_on_duplicate_array);

        # Подготовка к синхронизации файлов на диске, проверка директории
        $dirname = "/" . trim($files_dir, "/") . "/";
        $basePath = $dirname;
        if (!is_dir($basePath)) {
            mkdir($basePath);
        }
        if (!is_writable($basePath)) {
            throw new Exception($basePath . ' is not writeable');
        }

        /**
         * Удаляем изменившиеся файлы
         */
        foreach ($toInsert as $file) {
            if (isset($exists_vals[$file['id']]) && $exists_vals[$file['id']]['date_added'] <> $file['date_added']) {
                $filename = $exists_vals[$file['id']]['file_url'];
                if (is_file($basePath . $filename)) {
                    @unlink($basePath . $filename);
                }
            }
        }

        $updated = 0;
        # Производим добавление и обновление записей в базе
        foreach ($toInsert as $toInsert_item_array) {

            # Синхронизируем файлы на диске
            $dir = $toInsert_item_array['estate_id'];

            if (!is_dir($basePath . $dir)) {
                mkdir($basePath . $dir);
                if (!is_writable($basePath . $dir)) {
                    throw new Exception($basePath . $dir . ' is not writeable');
                }
            }
            $filename = $dir . "/" . $toInsert_item_array['id'] . '.' . $toInsert_item_array['file_ext'];

            if (is_file($basePath . $filename) && isset($exists_vals[$toInsert_item_array['id']]) && filesize($basePath . $filename) > 0) {
                $toInsert_item_array['file_url'] = $filename;
            } else {
                if (is_file($basePath . $filename)) {
                    @unlink($basePath . $filename);
                }


                $pNew = fopen($basePath . $filename, 'w');
                if (!is_resource($pNew)) {
                    throw new Exception($basePath . $filename . ' is not writeable');
                }

                $pOld = fopen($toInsert_item_array['file_url'], 'r');
                while (!feof($pOld)) {
                    fwrite($pNew, fread($pOld, 8192));
                }
                fclose($pNew);
                fclose($pOld);

                if (filesize($basePath . $filename) > 0) {
                    $toInsert_item_array['file_url'] = $filename;
                }
            }

            # Теперь добавляем и обновляем записи уже с внутренними путями к файлам
            $toInsert_item_array_slashed = array();
            foreach ($toInsert_item_array as $toInsert_item_array_item) {
                $toInsert_item_array_slashed[] = addslashes($toInsert_item_array_item);
            }
            $toInsert_item = '"' . implode('","', $toInsert_item_array_slashed) . '"';
            $updated = $this->dbhandler->exec("insert into " . $table . " (" . $fields . ") VALUES (" . $toInsert_item . ") ON DUPLICATE KEY UPDATE " . $toUpdate_items_on_duplicate);
        }
        $this->debug('updated ' . $updated);
    }
}