<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 02.03.2017
 * Time: 20:36
 */
namespace Macrocrm;

class Order
{

    /**
     * @var Client
     */
    private $client;

    protected $utm_fields = [
        'utm_keyword',
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_type',
        'utm_block',
        'utm_position',
        'utm_campaign_id',
        'utm_ad_id',
        'utm_phrase_id',
        'ip',
        'user_agent'
    ];

    function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Добавляет в CRM новое объявление о покупке/продаже недвижимости
     *
     * @param array $params
     */

    /**
     * @param string $contact_name Имя контакта
     * @param string $contact_phone Телефон контакта
     * @param string $message Сообщение от контакта (интерес)
     * @param bool   $novostroy Запрос по новостройке
     * @param string $activity Купить или продать
     * @param array  $utm_params UTM метки
     *
     * @return mixed|null
     */


    /**
     * Добавляет в CRM новое объявление о покупке/продаже недвижимости
     *
     * @param        $contact_name - имя контакта
     * @param        $contacts_phones - телефон(ы) контакта
     * @param string $message - сообщение от клиента
     * @param array  $params - все остальные возможные параметры, включая UTM метки
     *
     * Варианты полей для передачи в $params
     * - activity
     *      'sell': заявка на продажу объекта
     *      'buy': заявка на покупку объекта  (по-умолчанию)
     * - type
     *      'living': заявка на жилую недвижимость (включая парковки, кладовки, гаражи)  (по-умолчанию)
     *      'comm': заявка на коммерческую недвижимость
     * - category
     *      'flat': квартира (по-умолчанию)
     *      'garage': гараж/парковка
     *      'house': дом/таунхаус
     *      'storageroom': кладовка
     *      'comm': коммерческое помещение
     * - estate_living_new
     *      true: заявка на покупку новостройки (по-умолчанию)
     *      false: заявка на покупку вторички
     *
     * - email: передача email клиента (вместе либо вместо телефона)
     * - ip
     * - user_agent
     * - cookie_base64: base64_encode($_COOKIE)
     * - channel_medium: имя формы на сайте или акции по которой отправлена заявка
     *
     *
     * @return mixed|null
     */
    function add($contact_name, $contacts_phones, $message = '', $params = [])
    {

        $activity = isset($params['activity']) && in_array($params['activity'], ['sell', 'buy']) ? $params['activity'] : 'buy';
        $type = isset($params['type']) && in_array($params['type'], ['living', 'comm']) ? $params['type'] : 'living';
        $category = isset($params['category']) && in_array($params['category'], ['flat', 'comm', 'house', 'storageroom', 'garage']) ? $params['category'] : 'flat';
        if ($category == 'comm') {
            $type = 'comm';
        }

        return $this->client->api('estate/addOrder', array(
            'type' => $type,
            'estate_activity' => $activity,
            'name' => $contact_name,
            'phone' => $contacts_phones,
            'email' => isset($params['email']) ? $params['email'] : null,
            'category' => $category,
            'estate_living_new' => isset($params['estate_living_new']) && $params['estate_living_new']===false ? 0 : 1,
            'utm' => $params,
            'message' => $message
        ));
    }
}