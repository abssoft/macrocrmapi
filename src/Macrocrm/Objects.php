<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 01.04.2017
 * Time: 22:02
 */

namespace Macrocrm;

use \PDO;
use \PDOStatement;

class Objects {

    /**
     * @var PDO
     */
    protected $dbhandler;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string Имя таблицы в которой будет храниться объекты
     */
    protected $_table_name;

    protected $debugFunc;

    /**
     * @var array схема данных основной таблицы объектов
     */
    private $init_ddl = array(
        'id' => 'int(11)',
        'parent_id' => 'int(11)',
        'complex_id' => 'int(11)',
        'has_parent_id' => 'int(1)',
        'type' => 'varchar(10)',
        'activity' => 'varchar(8)',
        'company_id' => 'int(11)',
        'company_name' => 'varchar(64)',
        'category' => 'varchar(16)',
        'title' => 'varchar(255)',
        'description' => 'text',
        'category_name' => 'varchar(32)',
        'date_modified' => 'int(11)',
        'date_published' => 'int(11)',
        'date_added' => 'int(11)',
        'manager_name' => 'varchar(64)',
        'manager_phones' => 'varchar(64)',
        'manager_avatar' => 'varchar(255)',
        'contact_name' => 'varchar(64)',
        'contact_phones' => 'varchar(64)',
        'developer_id' => 'int(11)',
        'developer_name' => 'varchar(64)',
        'title_image' => 'varchar(255)',
        'address' => 'varchar(255)',
        'estate_category_subtype_human' => 'varchar(255)',
        'is_reserved' => 'int(1)',
        'is_sold' => 'int(1)',
        'is_hot' => 'int(1)',
        'is_advert' => 'int(1)',
        'images' => 'text',
        'parent_tags'=> 'varchar(255)',
        'tags' => 'varchar(255)',
        'plan_id'=> 'int(11)',
        'plan_name' => 'varchar(32)',
        'plan_image' => 'varchar(255)'
    );

    function __construct(Client $client, PDO $PDO,$table_name='objects')
    {
        $this->client=$client;
        $this->dbhandler = $PDO;
        $this->_table_name=$table_name;
    }



    /**
     * Запись в таблицу данных объекта
     *
     * @param $row
     */
    protected function dbinsert($row)
    {


        $InsertStatement = $this->dbhandler->prepare("replace into " . $this->quoteIdent($this->_table_name) . " (" . implode(',', array_keys($row)) . ") values (:" . implode(',:', array_keys($row)) . ")");

        return $InsertStatement->execute($row);

    }

    /**
     * Экранирование данных
     *
     * @param $arr
     *
     * @return mixed
     */
    function sqlite_escape_array(&$arr)
    {
        foreach ($arr as $key => $val) {
            if (is_string($val)) {
                $arr[$key] = "'" . $this->dbhandler->quote($val) . "'";
            } elseif (is_array($val)) {
                $arr[$key] = $this->sqlite_escape_array($val);
            } else {
                $arr[$key] = "'" . $val . "'";
            }
        }
        return $arr;
    }

    /**
     * Проверка наличия таблицы в базе
     *
     * @param string $table_name
     *
     * @return bool
     */
    function tableExists($table_name)
    {
        $mrSql = "SHOW TABLES LIKE :table_name";
        $mrStmt = $this->dbhandler->prepare($mrSql);
        //protect from injection attacks
        $mrStmt->bindParam(":table_name", $table_name, PDO::PARAM_STR);

        $sqlResult = $mrStmt->execute();
        if ($sqlResult) {
            $row = $mrStmt->fetch(PDO::FETCH_NUM);
            if ($row[0]) {
                //table was found
                return true;
            } else {
                //table was not found
                return false;
            }
        } else {
            //some PDO error occurred
            return false;
        }
    }

    /**
     *  Удаление таблицы
     */
    protected function dropTable($table_name)
    {
        $this->dbhandler->exec("DROP TABLE IF EXISTS " . $this->quoteIdent($table_name));
    }

    /**
     * Получение существующей схемы основной таблицы
     *
     * @return array
     */
    protected function get_columns($table_name = null)
    {
        if (is_null($table_name)) {
            $table_name = $this->_table_name;
        }
        $cols = array();
        foreach ($this->query('show columns from ' . $this->quoteIdent($table_name)) as $col) {
            $cols[$col['Field']] = preg_replace('~[^a-z ]~', '', $col['Type']);
        }
        return $cols;
    }

    /**
     * Синнхронизация схемы основной таблицы
     * При наличии изменений пересоздает
     */
    function sync($options = array(), $blank = false)
    {
        if (!is_array($options)) {
            $options = array();
        }
        /*
         * Получение схемы аттрибутов объекта
         */
        $data = $this->client->api('estate/get_schema', $options);

        if (!is_array($data)) {
            throw new Exception('No data to sync DB schema');
        }

        foreach ($data as $key => $type) {
            if (strpos($key,'_human')===false) {
                if (in_array($key, ['estate_category_type', 'estate_category_subtype'])) {
                    $data[$key . '_human'] = 'varchar(255)';
                } elseif (!in_array($type, ['bool', 'varchar','int','decimal']) || $key == 'geo_house') {
                    $data[$key . '_human'] = 'varchar(32)';
                } elseif ($type == 'varchar') {
                    $data[$key] = 'varchar(32)';
                }
            } elseif ($type == 'varchar') {
                $data[$key] = 'varchar(32)';
            }
        }
        //мержим схемы и перезаписываем типы знаковых полей из схемы
        $data = array_merge($this->init_ddl, $data, $this->init_ddl);
        $cols = $blank ? array() : $this->get_columns();
        $to_delete = array_diff(array_keys($cols), array_keys($data));
        $to_insert = array_diff(array_keys($data), array_keys($cols));


        /*
         * Если есть существенные изменения - пересоздаем таблицу
         */
        if (!empty($to_delete) or !empty($to_insert)) {

            $this->dropTable($this->_table_name);

            $cols = array();
            foreach ($data as $col => $type) {
                if ($col == 'id') {
                    $type = 'int(11) unsigned NOT NULL';
                } elseif ($type == 'int') {
                    $type = 'INT(11)';
                } elseif ($type == 'decimal') {
                    $type = 'DECIMAL(16,4)';
                } elseif ($type == 'bool') {
                    $type = 'TINYINT(1) unsigned';
                } elseif ($type != 'text' && strpos($type, '(') === false) {
                    $type = 'VARCHAR(48)';
                }

                $cols[$col] = $this->quoteIdent($col) . ' ' . $type;
            }
            $ddl = "CREATE TABLE " . $this->quoteIdent($this->_table_name) . " (" . implode(',', $cols) . ", "
                . "PRIMARY KEY (id)"
                . ",KEY " . $this->quoteIdent('idx_type_act') . " (type,activity)"
                . ",KEY " . $this->quoteIdent('idx_sold') . " (is_sold)"
                . ",KEY " . $this->quoteIdent('idx_category') . " (category)"
                . (isset($cols['estate_rooms']) ? ",KEY " . $this->quoteIdent('idx_estate_rooms') . " (estate_rooms)" : '')
                . ",KEY " . $this->quoteIdent('idx_geo_city') . " (geo_city)"
                . ",KEY " . $this->quoteIdent('idx_geo_region') . " (geo_region)"
                . ") ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC";


            $this->dbhandler->exec($ddl);
        }

        $this->sync_stat();
    }

    protected function quoteIdent($field)
    {
        return "`" . str_replace("`", "``", $field) . "`";
    }

    /**
     * Инициализация таблицы статистики
     */
    function sync_stat()
    {

        $stat_table_name = $this->_table_name . '_stat';

        if (!$this->tableExists($stat_table_name)) {
            $this->dropTable($stat_table_name);
            $ddl = 'CREATE TABLE ' . $this->quoteIdent($stat_table_name) . ' (id int(11) unsigned NOT NULL,stat varchar(255) NOT NULL, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8';
            $this->dbhandler->exec($ddl);
        }
    }

    /**
     * Получение последней даты модификации записей
     *
     * @return integer
     */
    function get_last_modified($type = null)
    {
        return (integer)$this->query_one('select max(date_modified) from ' . $this->quoteIdent($this->_table_name) . ($type ? " where type=" . $this->dbhandler->quote($type) : ''));
    }

    /**
     * Обновление записей объектов
     *
     * @param array $options
     */
    function update_records($options = array())
    {

        if (!$this->tableExists($this->_table_name)) {
            $this->debug('Syncing table ' . $this->_table_name . '...');
            $this->sync($options, true);
        }

        if (!is_array($options)) {
            $options = array();
        }

        if (!isset($options['type'])) {
            $options['type'] = 'living';
        }

        if (!isset($options['last_modified'])) {
            $options['last_modified'] = $this->get_last_modified($options['type']);
        }

        $options['limit'] = 100;

        $options['start_from'] = 0;



        /*
         * Помечаем все объекты как неизмененные, для последующего удаления из базы устаревших
         */
        if ($options['last_modified'] == 0) {
            $this->dbhandler->prepare('update ' . $this->quoteIdent($this->_table_name) . ' set date_modified=0 where type=:type')->execute(array(':type' => $options['type']));
        }

        $cols = $this->get_columns();
        foreach ($cols as &$v) {
            $v = '';
        };

        $InsertStatement = $this->dbhandler->prepare("replace into " . $this->quoteIdent($this->_table_name) . " (" . implode(',', array_keys($cols)) . ") values (:" . implode(',:', array_keys($cols)) . ")");

        /*
         * Получаем новые объекты
         */
        do {


            $records = $this->client->api('estate/get', $options);
            $this->debug($this->client->last_url_fetched);

            $continue = false;


            $fetched = 0;
            $removed = 0;
            $this->debug(str_repeat(' ', 40) . "\r");
            $this->debug('start from:' . $options['start_from'] . ' ');

            if (count($records)) {

                reset($records);

                while (list(, $record) = each($records)) {


                    if (isset($record['last_record_id'])) {
                        $options['start_from'] = $record['last_record_id'];

                        $continue = true;
                        break;
                    }
                    if (empty($record['removed'])) {

                        if (isset($record['images']) && is_array($record['images'])) {
                            $record['images'] = json_encode($record['images']);
                        }

                        $InsertStatement->execute(array_merge($cols, array_intersect_key($record, $cols)));
                        $fetched++;

                    } else {


                        $this->dbhandler->exec('delete from ' . $this->quoteIdent($this->_table_name) . ' where id=' . $record['id']);
                        $removed++;

                    }

                }


            }

            if (!$fetched && !$removed) {
                $this->debug('no data!' . "\r");
                $this->debug($this->client->last_url_fetched . "\r");
                $this->debug($this->client->last_data_fetched . "\r");
                return;
            } else {
                $this->debug('fetched:' . $fetched . ", ");
                $this->debug('removed:' . $removed . "\r");
            }

            if (!$continue) sleep(1);

        } while ($continue === true);


        if ($options['last_modified'] == 0) {
            $stm = $this->dbhandler->prepare('delete from ' . $this->quoteIdent($this->_table_name) . ' where date_modified=0 and type=:type');
            $stm->execute(array(':type' => $options['type']));
            $this->debug('remove not modified: ' . $stm->rowCount() . "\r");
        }

    }

    /**
     * Обновление одного объекта
     *
     * @param array $options id,hash - ключ доступа к объекту, генерируется API
     *
     * @return bool
     */
    function update_one($options = array())
    {
        if (!isset($options['id'], $options['hash'])) {
            return trigger_error('not enough params');
        }
        $records = $this->client->api('estate/get', $options);

        if (isset($records[0]) && $records[0]['id'] == $options['id']) {


            $record = $records[0];
            if (!empty($record['removed'])) {
                $this->dbhandler->exec('delete from ' . $this->quoteIdent($this->_table_name) . ' where id=' . $record['id']);
            } else {
                $cols = $this->get_columns();

                if (isset($record['images']) && is_array($record['images'])) $record['images'] = json_encode($record['images']);
                $this->dbinsert(array_intersect_key($record, $cols));
            }

            return true;
        }
        return false;

    }


    /**
     * Прямой запрос в базу данных
     *
     * @param $query
     *
     * @return array
     */
    function query($query)
    {
        $data = $this->dbhandler->query($query);
        return $data == false ? array() : $data->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Запрос единственного значения из базы
     *
     * @param $query
     *
     * @return string
     */
    function query_one($query, $data = null)
    {
        $stm = $this->dbhandler->prepare($query);
        return $stm->execute($data) ? $stm->fetchColumn(0) : null;
    }

    /**
     * Возвращает запись объекта
     *
     * @param $id
     *
     * @return array|null
     */
    function getRecord($id)
    {
        $data = $this->query('select * from ' . $this->quoteIdent($this->_table_name) . ' where id=' . (0 + $id));
        if (isset($data[0]['images'])) {
            $data[0]['images'] = json_decode($data[0]['images'], true);
        }
        return isset($data[0]) ? $data[0] : null;
    }

    /**
     * Возвращает все записи объектов
     *
     * @return array
     */
    function getAllRecords()
    {
        $data = $this->query('select * from ' . $this->quoteIdent($this->_table_name) . ' order by date_modified desc');
        if (!is_array($data)) $data = array();
        foreach ($data as &$rec) {
            if (isset($rec['images'])) {
                $rec['images'] = json_decode($rec['images'], true);
            }
        }
        return $data;
    }

    /**
     * @param string $req : the query on which link the values
     * @param array  $array : associative array containing the values ​​to bind
     * @param array  $typeArray : associative array with the desired value for its corresponding key in $array
     *
     * @return PDOStatement
     **/
    function bindArrayValue($req, &$array, $typeArray = false)
    {
        if (is_object($req) && ($req instanceof PDOStatement)) {
            foreach ($array as $key => &$value) {
                if ($typeArray)
                    $req->bindValue(":$key", $value, $typeArray[$key]);
                else {
                    if (is_int($value))
                        $param = PDO::PARAM_INT;
                    elseif (is_bool($value))
                        $param = PDO::PARAM_BOOL;
                    elseif (is_null($value))
                        $param = PDO::PARAM_NULL;
                    elseif (is_string($value))
                        $param = PDO::PARAM_STR;
                    else
                        $param = FALSE;

                    if ($param)
                        $req->bindParam(":$key", $value, $param);
                }
            }
        }
        return $req;
    }


    /**
     * Возвращает экскиз изображения
     *
     * @param $record array запись объекта
     *
     * @return bool|string
     */
    function getThumbImage($record)
    {
        if (!empty($record['title_image'])) {
            $img = $record['title_image'];
            return dirname($img) . '/thumb/' . basename($img);
        } elseif (isset($record['images'])) {
            if (!is_array($record['images'])) {
                $record['images'] = json_decode($record['images'], true);
            }
            if (isset($record['images'][0]['images'][0])) {
                $img = $record['images'][0]['images'][0]['file_url'];
                return dirname($img) . '/thumb/' . basename($img);
            }
        }
        return false;
    }


    function debug()
    {
        $data = func_get_args();
        if (is_callable($this->debugFunc)) {
            call_user_func_array($this->debugFunc, $data);
        }
    }

    function setDebug($callable)
    {
        $this->debugFunc = $callable;
    }
}