<?php
/**
 * Created by Artem Breslavskiy (abssoft)
 * Date: 01.04.2017
 * Time: 22:02
 */

namespace Macrocrm;

use \PDO;
use \PDOStatement;

class Hod {

    /**
     * @var PDO
     */
    protected $dbhandler;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string Имя таблицы в которой будет храниться объекты
     */
    protected $_table_name;

    protected $debugFunc;

    public $trash_folder = false;

    function __construct(Client $client, PDO $PDO,$table_name='objects_hod_files')
    {
        $this->client=$client;
        $this->dbhandler = $PDO;
        $this->_table_name=$table_name;
    }



    /**
     * Проверка наличия таблицы в базе
     *
     * @param string $table_name
     *
     * @return bool
     */
    function tableExists($table_name)
    {
        $mrSql = "SHOW TABLES LIKE :table_name";
        $mrStmt = $this->dbhandler->prepare($mrSql);
        //protect from injection attacks
        $mrStmt->bindParam(":table_name", $table_name, PDO::PARAM_STR);

        $sqlResult = $mrStmt->execute();
        if ($sqlResult) {
            $row = $mrStmt->fetch(PDO::FETCH_NUM);
            if ($row[0]) {
                //table was found
                return true;
            } else {
                //table was not found
                return false;
            }
        } else {
            //some PDO error occurred
            return false;
        }
    }

    /**
     *  Удаление таблицы
     */
    protected function dropTable($table_name)
    {
        $this->dbhandler->exec("DROP TABLE IF EXISTS " . $this->quoteIdent($table_name));
    }

    /**
     * Получение существующей схемы основной таблицы
     *
     * @return array
     */
    protected function get_columns($table_name = null)
    {
        if (is_null($table_name)) {
            $table_name = $this->_table_name;
        }
        $cols = array();
        foreach ($this->query('show columns from ' . $this->quoteIdent($table_name)) as $col) {
            $cols[$col['Field']] = preg_replace('~[^a-z ]~', '', $col['Type']);
        }
        return $cols;
    }


    /**
     * Синнхронизация схемы основной таблицы
     * При наличии изменений пересоздает
     * Сохранение файлов CRM на диске
     * Добавление и обновление элементов
     */
    function sync_hod($files_dir)
    {
        $table = $this->_table_name;
        # Если таблица не существует - создаем
        $this->debug($table.' syncing...');
        if (!$this->tableExists($table)) {
            $this->dropTable($table);
            $ddl = 'CREATE TABLE ' . $this->quoteIdent($table) . ' (
            id int(11) unsigned NOT NULL,
            estate_id int(11) unsigned NOT NULL,
            contacts_id int(11) unsigned NOT NULL,
            geo_city varchar(32) NOT NULL,
            geo_street varchar(48) NOT NULL,
            geo_house varchar(16) NOT NULL,
            geo_complex varchar(48) NOT NULL,
            contacts_name varchar(64) NOT NULL,
            file_name varchar(255) NOT NULL,
            file_url varchar(255) NOT NULL, 
            date_ym varchar(7) NOT NULL,            
            PRIMARY KEY (id), INDEX(estate_id),INDEX(contacts_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8';
            $this->dbhandler->exec($ddl);
        }

        # Тянем данные из API
        $data = $this->client->api('estate/group/getHodPhotos');
        if (empty($data)) {
            if (is_array($data)) {
                $this->debug('Empty data from server');
                return false;
            }
            throw new Exception('Server returned no data');
        }

        # Получаем поля таблицы в базе
        $columns = $this->get_columns($table);

        # Собираем ID всех записей, имеющихся в базе
        $exists_query = $this->dbhandler->query("select id from " . $table . " order by null");
        $exists = $exists_query->fetchAll();

        # Приводим собранные ID к удобному виду
        $exists_vals = array();
        foreach ($exists as $exists_val) {
            $exists_vals[] = (int)$exists_val['id'];
        }

        # Инициализируем массив записей для добавления в базу, где ключом является ID
        $toInsert = array();
        foreach ($data as $file) {
            $toInsert[$file['id']] = array_intersect_key($file, $columns);
        }

        # Прерываем выполнение, если данные не пришли
        if (empty($toInsert)) {
            $this->debug('no date');
            return false;
        }

        # Формируем массив ID записей для удаления и удаляем из базы лишнее
        $toDelete = array_diff($exists_vals, array_keys($toInsert));
        if (!empty($toDelete)) {
            $toDelete_string = implode(',', $toDelete);
            $delete_query = "delete from " . $table . " where id in (" . $toDelete_string . ")";
            $this->dbhandler->exec($delete_query);
        }


        # Обновляем записи в базе

        # Формируем поля для вставки в запрос
        $fields = '`' . implode('`,`', array_keys(reset($toInsert))) . '`';

        # Формируем строку для вставки в запрос при обновлении элемента
        $toUpdate_items_on_duplicate_array = array();
        $toUpdate_items = array_keys(reset($toInsert));
        foreach ($toUpdate_items as $toUpdate_item) {
            $toUpdate_items_on_duplicate_array[] = "`" . $toUpdate_item . "`=VALUES(`" . $toUpdate_item . "`)";
        }
        $toUpdate_items_on_duplicate = implode(' ,', $toUpdate_items_on_duplicate_array);

        # Подготовка к синхронизации файлов на диске, проверка директории
        $basePath = rtrim($files_dir, "/") . "/";
        if (!is_dir($basePath)) {
            mkdir($basePath);
        }
        if (!is_writable($basePath)) {
            throw new Exception($basePath . ' is not writeable');
        }

        # Производим добавление и обновление записей в базе
        foreach ($toInsert as $toInsert_item_array) {

            # Синхронизируем файлы на диске
            $dir = $toInsert_item_array['estate_id'];

            if (!is_dir($basePath . $dir)) {
                mkdir($basePath . $dir);
                if (!is_writable($basePath . $dir)) {
                    throw new Exception($basePath . $dir . ' is not writeable');
                }
            }
            $filename = $dir . "/" . $toInsert_item_array['id'] . '.' . pathinfo($toInsert_item_array['file_url'], PATHINFO_EXTENSION);

            if (is_file($basePath . $filename) && in_array($toInsert_item_array['id'], $exists_vals) && filesize($basePath . $filename) > 1000){
                $toInsert_item_array['file_url'] = $filename;
            } else {
                if (is_file($basePath . $filename)){
                    @unlink($basePath . $filename);
                }


                $pNew = fopen($basePath . $filename, 'w');

                $pOld = fopen($toInsert_item_array['file_url'], 'r');
                while (!feof($pOld)) {
                    fwrite($pNew, fread($pOld, 8192));
                }
                fclose($pNew);
                fclose($pOld);

                if (filesize($basePath . $filename) > 1000) {
                    $toInsert_item_array['file_url'] = $filename;
                }
            }

            # Теперь добавляем и обновляем записи уже с внутренними путями к файлам
            $toInsert_item_array_slashed = array();
            foreach ($toInsert_item_array as $toInsert_item_array_item) {
                $toInsert_item_array_slashed[] = addslashes($toInsert_item_array_item);
            }
            $toInsert_item = '"' . implode('","', $toInsert_item_array_slashed) . '"';
            $updated = $this->dbhandler->exec("insert into " . $table . " (" . $fields . ") VALUES (" . $toInsert_item . ") ON DUPLICATE KEY UPDATE " . $toUpdate_items_on_duplicate );
        }
        $this->debug('updated '.$updated);

        # Удаляем лишние файлы
        $this->remove_outdated_files($basePath);
    }

    /**
     * Удаляем лишние файлы
     */
    function remove_outdated_files($basePath)
    {
        # Массив актуальных файлов, где ключами являются пути
        $actual_files_array = [];

        # Пополняем массив актуальными путями
        foreach ($this->query('select file_url from ' . $this->_table_name . ' group by file_url') as $actual_file) {
            $actual_files_array[$actual_file['file_url']] = true;
        }

        # Выделяем пути к файлам для перемещения в корзину
        foreach ($this->dir_to_files($basePath, [$this->trash_folder]) as $dirTofile) {
            if(!isset($actual_files_array[$dirTofile])) {
                $new_dir = dirname($basePath . $this->trash_folder . '/' . $dirTofile);
                if (!is_dir($new_dir)) {
                    mkdir($new_dir, 0777, true);
                    if (!is_writable($new_dir)) {
                        throw new Exception($new_dir . ' is not writeable');
                    }
                }
                if($this->trash_folder) {
                    rename($basePath . $dirTofile, $basePath . $this->trash_folder . '/' . $dirTofile);
                }
                else {
                    unlink($basePath . $dirTofile);
                }
            }
        }

        # Удаляем пустые папки
        $this->rmdir_empty($basePath);
    }

    /**
     * Удаляем пустые папки
     */
    function rmdir_empty($root, $remove_root = false)
    {
        $empty = true;
        $root = realpath($root);
        if ($root AND is_dir($root))
        {
            foreach (scandir($root) AS $file)
            {
                if ($file == '.' OR $file == '..')
                    continue;

                $empty = false;

                if (is_dir($root . DIRECTORY_SEPARATOR . $file))
                    $this->rmdir_empty($root . DIRECTORY_SEPARATOR . $file, true);
            }

            if ($remove_root && $empty) {
                @rmdir($root);
            }
        }
    }

    /**
     * Получаем список файлов и каталогов
     */
    function dir_to_files($dir, $ignore) {

        $result = [];

        $cdir = scandir($dir);
        foreach ($cdir as $key => $value)
        {
            if (!in_array($value, array_merge([".",".."], $ignore)))
            {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
                {
                    foreach ($this->dir_to_files($dir . DIRECTORY_SEPARATOR . $value, $ignore) as $string_value) {
                        $result[] = $value . '/' . $string_value;
                    }
                }
                else
                {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    /**
     * Прямой запрос в базу данных
     *
     * @param $query
     *
     * @return array
     */
    function query($query)
    {
        $data = $this->dbhandler->query($query);
        return $data == false ? array() : $data->fetchAll(PDO::FETCH_ASSOC);
    }

    protected function quoteIdent($field)
    {
        return "`" . str_replace("`", "``", $field) . "`";
    }

    function debug()
    {
        $data = func_get_args();
        if (is_callable($this->debugFunc)) {
            call_user_func_array($this->debugFunc, $data);
        }
    }

    function setDebug($callable)
    {
        $this->debugFunc = $callable;
    }
}