#Deprecated

### API для подключения к MacroCRM

### Пример создания объявления в CRM о покупке недвижимости   

```php

# подготавливаем клиента
$client = new \Macrocrm\Client($your_domain,$your_app_secret);
  
$Order = new \Macrocrm\Order($client);
$result = $Order->add('Иван Иванов','8(555)555-55-55','Интересуют 3-комнатные, до 2 млн.',
[
    'utm_keyword'=>'новостройки самары',
    'utm_source'=>'yandex',
    'utm_medium'=>'cpc',
    'utm_campaign'=>'kupit_kvartiru',
    'utm_type'=>'context',
    'utm_block'=>'premium',
    'utm_position'=>'3',
    'utm_campaign_id'=>'12109414',
    'utm_ad_id'=>'794480184',
    'utm_phrase_id'=>'3203266908',
    'ip'=>'92.123.123.92',
    'user_agent'=>'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36 OPR/28.0.1750.51 (Edition Yx)',
    'cookie_base64'=>base64_encode(json_encode($_COOKIE)),
    'channel_medium'=>'Ипотека'
]);

# в случае удачного создания возвращается ID созданного объявления
# иначе сообщение об ошибке
if (isset($result['success'])){
    $order_id=$result['estate_id'];
} elseif (isset($result['error'])){
    $errorMsg = $result['message'];
}
```



### Пример синхронизации локальных объектов из MacroCRM

```php

# подготавливаем клиента
$client = new \Macrocrm\Client($your_domain,$your_app_secret);

# подготавливаем PDO для доступа к базе данных
$db = new PDO('dblib:host=your_hostname;dbname=your_db;charset=UTF-8', $user, $pass);

# настраиваем вывод исключений при ошибках
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
# имя таблицы с объектами в вашей базе данных
# таблица будет создана при синхронизации
$table_name = 'objects';

$Objects = new \Macrocrm\Objects($client, $db, $table_name);
$type = 'living';//для обновления жилой недвижимости
//$type = 'comm';//для обновления коммерческой недвижимости

$Objects->update_records([
    'type' => $type, 
    'last_modified' => 0, 
    'class' => $type == 'living' ? 'estate' : 'commestate', 
    'schema' => 'estate:' . $type, 
    'activity' => $type == 'living' ? array('sell') : array('sell', 'rent')
    ]);

#получение записи объекта по его ID
$record = $Objects->getRecord($id);

# получение всего списка объектов вы можете выполнять стандартными средствами
# доступа к базе данных в вашей системе
```



### Пример синхронизации хода строительства объектов из MacroCRM

```php

# подготавливаем клиента
$client = new \Macrocrm\Client($your_domain, $your_app_secret);
 
# подготавливаем PDO для доступа к базе данных
$db = new PDO('mysql:host=your_hostname;dbname=your_db;charset=UTF8', $user, $pass);
 
# настраиваем вывод исключений при ошибках
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
# имя таблицы с объектами в вашей базе данных
# таблица будет создана при синхронизации
$table_name = 'your_table_name';
 
$Hod = new \Macrocrm\Hod($client, $db, $table_name);
 
# Имя папки корзины, не обязательный параметр,
# указывается в случае если нужно не удалять лишние файлы,
# а помещать в отдельную папку
$Hod->trash_folder = $trash_folder_name;
 
# Производим синхронизацию, указав директорию для хранения файлов
$Hod->sync_hod('your_hod_files_directory');
```



### Пример синхронизации документов (проектные декларации и т.д.) объектов строительства из MacroCRM
```php

# подготавливаем клиента
$client = new \Macrocrm\Client($your_domain,$your_app_secret);
 
# подготавливаем PDO для доступа к базе данных
$db = new PDO('mysql:host=your_hostname;dbname=your_db;charset=UTF8', $user, $pass);
 
# настраиваем вывод исключений при ошибках
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
# имя таблицы с ТМЦ в вашей базе данных
# таблица будет создана при синхронизации
$table_name = 'your_table_name';
 
$Documents = new \Macrocrm\Documents($client,$db,$table_name);

# полный путь до локальной директории, где будут храниться файлы документов
$dir = '/some/path/to/docs';
 
# вызываем метод для получения данных по текущей компании
$Documents->sync_documents($dir);

```


### Пример получения изображений и разметки фасадов из MacroCRM

```php

# подготавливаем клиента
$client = new \Macrocrm\Client($your_domain, $your_app_secret);
 
# подготавливаем PDO для доступа к базе данных
$db = new PDO('mysql:host=your_hostname;dbname=your_db;charset=UTF8', $user, $pass);
 
# настраиваем вывод исключений при ошибках
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
$Facades = new \Macrocrm\Facades($client, $db);

# полный путь до локальной директории, где будут храниться изображения и разметка фасадов
$dir = '/some/path/to/facades';

# Производим синхронизацию
$Facades->sync($dir);
```


### Пример получения изображений и разметки планов этажей из MacroCRM

```php

# подготавливаем клиента
$client = new \Macrocrm\Client($your_domain, $your_app_secret);
 
# подготавливаем PDO для доступа к базе данных
$db = new PDO('mysql:host=your_hostname;dbname=your_db;charset=UTF8', $user, $pass);
 
# настраиваем вывод исключений при ошибках
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
$FloorPlans = new \Macrocrm\FloorPlans($client, $db);

# полный путь до локальной директории, где будут храниться изображения и разметка планов этажей
$dir = '/some/path/to/floorplans';

# Производим синхронизацию
$FloorPlans->sync($dir);
```



### Пример получения данных по ТМЦ из MacroCRM
                
```php

# подготавливаем клиента
$client = new \Macrocrm\Client($your_domain,$your_app_secret);
 
# подготавливаем PDO для доступа к базе данных
$db = new PDO('mysql:host=your_hostname;dbname=your_db;charset=UTF8', $user, $pass);
 
# настраиваем вывод исключений при ошибках
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
# имя таблицы с ТМЦ в вашей базе данных
# таблица будет создана при синхронизации
$table_name = 'your_table_name';
 
# создаем экземпляр класса Inventory
$Inventory = new \Macrocrm\Inventory($client,$db,$table_name);
 
# вызываем метод для получения данных по текущей компании
$result = $Inventory->sync_inventory();

```
